FROM openjdk:17

VOLUME /tmp

EXPOSE 8080

COPY target/demo-0.0.1-SNAPSHOT.jar demo.jar

ENTRYPOINT ["java", "-jar", "demo.jar"]

# docker build -t demo-contenedor .
# docker run -p 8080:8080 -d demo-contenedor
