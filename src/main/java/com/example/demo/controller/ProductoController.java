package com.example.demo.controller;

import com.example.demo.model.Producto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductoController {
    private static List<Producto> listaProductos = new ArrayList<>();

    @GetMapping("/productos")
    public List<Producto> getProductos() {
        listaProductos.add(new Producto("Producto 1", "Seccion 1", 20, 2200, "Ubicacion 1"));
        listaProductos.add(new Producto("Producto 2", "Seccion 2", 35, 1000, "Ubicacion 2"));
        listaProductos.add(new Producto("Producto 3", "Seccion 3", 50, 12000, "Ubicacion 3"));
        listaProductos.add(new Producto("Producto 4", "Seccion 4", 12, 8000, "Ubicacion 4"));
        listaProductos.add(new Producto("Producto 5", "Seccion 5", 45, 5000, "Ubicacion 5"));
        listaProductos.add(new Producto("Producto 6", "Seccion 6", 22, 6500, "Ubicacion 6"));
        listaProductos.add(new Producto("Producto 7", "Seccion 7", 19, 3500, "Ubicacion 7"));
        listaProductos.add(new Producto("Producto 8", "Seccion 8", 8, 15000, "Ubicacion 8"));

        return listaProductos;
    }

}
