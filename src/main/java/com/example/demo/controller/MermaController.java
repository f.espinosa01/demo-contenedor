package com.example.demo.controller;

import com.example.demo.model.Merma;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MermaController {
    private static List<Merma> listaMerma = new ArrayList<>();

    @GetMapping("/mermas")
    public List<Merma> getMermas() {
        listaMerma.add(new Merma("Merma 1", "motivo 1", 10));
        listaMerma.add(new Merma("Merma 2", "motivo 2", 5));
        listaMerma.add(new Merma("Merma 3", "motivo 3", 8));
        return listaMerma;
    }

}
