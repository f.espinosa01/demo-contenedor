package com.example.demo.controller;

import com.example.demo.model.Usuario;
import com.example.demo.model.UsuarioCajero;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UsuarioCajeroController {
    private static List<UsuarioCajero> listaUsuariosCajeros = new ArrayList<>();

    @GetMapping("/usuariosCajeros")
    public List<UsuarioCajero> getUsuariosCajeros() {
        listaUsuariosCajeros.add(new UsuarioCajero("12345678-1", "Juan", "Juan", "juan1234", 26));
        listaUsuariosCajeros.add(new UsuarioCajero("18123789-2", "Maria", "Maria", "maria1234", 20));
        listaUsuariosCajeros.add(new UsuarioCajero("17456123-3", "Jose", "Jose", "jose1234", 33));
        listaUsuariosCajeros.add(new UsuarioCajero("11478124-4", "Francisco", "Francisco", "francisco1234", 18));
        listaUsuariosCajeros.add(new UsuarioCajero("13796138-5", "Andres", "Andres", "andres1234", 24));

        return listaUsuariosCajeros;
    }

}

