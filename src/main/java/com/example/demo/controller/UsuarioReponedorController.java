package com.example.demo.controller;

import com.example.demo.model.UsuarioReponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UsuarioReponedorController {
    private static List<UsuarioReponedor> listaUsuariosReponedor = new ArrayList<>();

    @GetMapping("/usuariosReponedores")
    public List<UsuarioReponedor> getUsuariosReponedores() {
        listaUsuariosReponedor.add(new UsuarioReponedor("12345678-1", "Manuel", "Manuel", "manuel1234", "1"));
        listaUsuariosReponedor.add(new UsuarioReponedor("18123789-2", "Carolina", "Carolina", "carolina1234", "2"));
        listaUsuariosReponedor.add(new UsuarioReponedor("17456123-3", "Benjamin", "Benjamin", "benjamin1234", "3"));

        return listaUsuariosReponedor;
    }

}

