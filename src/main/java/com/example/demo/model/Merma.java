package com.example.demo.model;

public class Merma {
    private String nombre;
    private String motivo;
    private Integer cantidad;

    public Merma(String nombre, String motivo, Integer cantidad) {
        this.nombre = nombre;
        this.motivo = motivo;
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}
