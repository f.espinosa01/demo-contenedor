package com.example.demo.model;

public class Producto {
    private String nombre;
    private String seccion;
    private Integer stock;
    private Integer precio;
    private String ubicacion;

    public Producto(String nombre, String seccion, Integer stock, Integer precio, String ubicacion) {
        this.nombre = nombre;
        this.seccion = seccion;
        this.stock = stock;
        this.precio = precio;
        this.ubicacion = ubicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
