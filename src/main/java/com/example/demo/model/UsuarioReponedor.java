package com.example.demo.model;

public class UsuarioReponedor extends Usuario {
    private String seccion;

    public UsuarioReponedor(String rut, String nombre, String usuario, String contrasenia, String seccion) {
        super(rut, nombre, usuario, contrasenia);
        this.seccion = seccion;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }
}
