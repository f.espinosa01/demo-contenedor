package com.example.demo.model;

public class UsuarioCajero extends Usuario{
    private Integer ventas;

    public UsuarioCajero(String rut, String nombre, String usuario, String contrasenia, Integer ventas) {
        super(rut, nombre, usuario, contrasenia);
        this.ventas = ventas;
    }

    public Integer getVentas() {
        return ventas;
    }

    public void setVentas(Integer ventas) {
        this.ventas = ventas;
    }
}
